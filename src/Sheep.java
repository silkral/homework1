import java.util.Arrays;

public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // for debugging
	   
   Animal[] anim = new Animal[] {Animal.sheep, Animal.goat, Animal.goat, Animal.sheep};
	   reorder (anim);
	   for(int i=0; i<anim.length; i++){
		   System.out.println(anim[i]);}
   }
   
   public static void reorder (Animal[] animals) {
      
	   
	   /**Variant 1. Listi läbikäimine alustades eest ja tagant; 
	    * kui ees on sheep ja taga goat, 
	    * siis vahetada indeksid;
	    * Koostatud järgmise allika baasil:
	    * http://stackoverflow.com/questions/25855106/
	    * how-to-sort-random-elements-of-an-array-efficiently-
	    * so-that-similar-items-appear
	    */
	
	int i = 0;//algusest lugemise kohaindeks
	int j = animals.length - 1;//lõpust lugemise kohaindeks
	Animal temp1, temp2;
	Animal[] temp = new Animal[animals.length];
	
	while (i<j){
		if (animals[i] ==Animal.sheep){//*kui ees sheep, 
			if(animals[j]==Animal.goat){//**ja taga goat
				temp1=animals[i];//siis vahetab 
				temp2=animals[j];
				animals[i]=temp2;
				animals[j]=temp1;
				j--;
				i++;
			} else {//**ja taga ei ole goat
				temp[i]=animals[j];
				j--;//siis astub sammu tagant ettepoole
				}
			} else {//*kui ees goat, siis astub edasi ühe võrra
			temp[i]=animals[i];
			i++; 
		}
	
	}

	
///**Variant 2: lugeda, kui palju sikke, kui palju lambaid ja teha uus massiiv selle järgi
//	    *
//	    */
//	   
//	   int loomadeArv = animals.length;
//	   int sikkudeArv = 0;
//	   Animal[] temp = new Animal[loomadeArv];
//	   
//	   for(Animal loom: animals)
//		   if(loom == Animal.goat) sikkudeArv++;  //sikkudeArv = sikkudeArv+1;
//	   
//	   
//	   for(int i=0; i<sikkudeArv;i++)
//		   temp[i]= Animal.goat; 
//	   
//	   for (int i=sikkudeArv; i<temp.length;i++)
//		   temp[i]= Animal.sheep;
//	   
//	   for(int i=0;i < temp.length;i++) //kopeerida väärtused temp massiivist algsesse massiivi tagasi
//           animals[i] = temp[i];
//	   	   
//	   

	 
	   //Variant 3
//	   /**1) seitakse sikkude arv; 
//	     *2) tehakse uus massiiv, eespool sikud, tagapool lambad
//	     * Koostatud alloleva allika baasil:
//	     * http://stackoverflow.com/questions/25852541/java-rarrange-enum-array
//	     */
//	   
//	   assert Animal.values().length==2;
//	   int numGoat = 0;
//	   for (Animal a:animals) if (a==Animal.goat) numGoat++;
//	   Arrays.fill(animals, 0, numGoat, Animal.goat);
//	   Arrays.fill(animals, numGoat, animals.length, Animal.sheep);
//	    	
	  
	 //Variant 4: alfabeetiline - väga aeglane???
//     int i, j;
//     Animal temp;
//
//     for ( i = 0;  i < animals.length - 1;  i++ )
//     {
//         for ( j = i + 1;  j < animals.length;  j++ )
//         { 
//                  if ( animals [ i ].compareTo( animals [ j ] ) < 0 )
//                   {                                             // ascending sort
//                               temp = animals [ i ];
//                               animals [ i ] = animals [ j ];    // swapping
//                               animals [ j ] = temp;
//                              
//                    }
//            }
//      }

   
	   
   }  
}
		
	

   


